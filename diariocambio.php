<?php
class cambio extends parse implements PInterface{
	/*
		Convierte la fecha de fromato cadena a date YYYY/MM/DD H:i:s
		@return String
	*/
	private function fecha2time($fecha){
		$meses = array('Enero'=>01,'Febrero'=>02,'Marzo'=>03,'Abril'=>04,'Mayo'=>05,'Junio'=>06,'Julio'=>07,'Agosto'=>08,'Septiembre'=>09,'Octubre'=>10,'Noviembre'=>11,'Diciembre'=>12);
		preg_match("/Salto, (Lunes|Martes|Miércoles|Jueves|Viernes|Sábado|Domingo) ([0-9]{2}) de (Enero|Febrero|Marzo|Abril|Mayo|Junio|Julio|Agosto|Septiembre|Octubre|Noviembre|Diciembre) de ([0-9]{4})/", $fecha, $output_array);
		return $output_array[4] . '-' .$meses[$output_array[3]] . '-' . $output_array[2] . '2:30:00';
	}
	/*
		Obtiene la fecha desde el archivo fuente
	*/
	public function  getFecha($totime){
		$this->getSource("www.diariocambio.com.uy");
		$xpath = $this->initialize();
		$fch = $xpath->query("//div[@id='fecha']")->item(0);
		//foreach ($fch->item(0)->childNodes as $fchchilds) {
			//if($fchchilds->nodeName == 'p'){
				$fecha = trim(htmlspecialchars($fch->nodeValue));
				//break;
			//}
		//}
		if(!$fecha){
			throw new Exception('Formato de origen desconocido', E_USER_ERROR);
		}
		if($totime)
			return strtotime($this->fecha2time($fecha));
		return $this->fecha2time($fecha);
	}
	public function getIndex(){
		$i=0;
		$xpath = $this->initialize();
		$c_n = $xpath->query("//div[@class='contenido_noticia']");
		foreach ($c_n as $childs) {
			foreach ($childs->childNodes as $value) {
				if($value->nodeName == 'p'){
					$this->data[$i]['body_short'] = trim(htmlspecialchars(utf8_decode($value->nodeValue)));
				}
				if($value->nodeName == 'div'){
					foreach ($value->childNodes as $v) {
						if($v->nodeName == 'a'){
							$this->data[$i]['link'] =  'http://www.diariocambio.com.uy/' . $v->getAttribute('href');
						}
					}		
				}
			}
			$i++;
		}
	}
	public function generate(){
		$as = array();
		$d = date("Y-m-d",strtotime($this->date));
		/*$this->getSource("www.diariocambio.com.uy/?fechaedicion=$d");
		$this->getIndex();
		foreach($this->data as $data){
			$as[] = array_merge($this->getArticle($data['link']),$data);
		}*/
		foreach ($this->getArticlesFromSections() as $value) {
			//$as[] = array_merge($this->getArticle($data['link']), $this->getArticle($value['link']));
			$as[] =  array_merge($value,$this->getArticle($value['link']));
		}
		return $as;
	}
	public function getArticle($url){
		$d = date("Y-m-d",strtotime($this->date));
		$this->getSource($url."&fechaedicion=$d");
		file_put_contents("urls.txt", $url."\r\n", FILE_APPEND);
		$xpath = $this->initialize();
		$contenido_noticia = $xpath->query("//div[@class='contenido_noticia']")->item(0);
		$title = $xpath->query("h2", $contenido_noticia)->item(0);
		$articulo['title'] = htmlspecialchars(utf8_decode($title->nodeValue));
		$noticia = $xpath->query("div[@class='noticia']", $contenido_noticia)->item(0);
		$foto = @$xpath->query("div[@class='foto']",$noticia)->item(0);
		if($foto){
			$articulo['img'] = 'http://www.diariocambio.com.uy/' . $foto->childNodes->item(0)->getAttribute('src');
		}
		$p =  $xpath->query("p", $noticia)->item(0);
		preg_match("/([\w]*)  \| ([\d]*) ([\w]*)/i", $p->textContent,$match);
		//
		$articulo['section'] =  trim($match[1]);
		$articulo['body'] = trim(preg_replace("/([\w]*)  \| ([\d]*) ([\w]*)/i", "", utf8_decode($p->textContent)));
		$articulo['date'] = $this->date;
		return $articulo;
	}
	public function getArticlesFromSections(){
		$i=0;
		$sections = ['locales','nacionales','policiales','deportes','politica','rurales'];
		foreach ($sections as $section) {
			$this->getSource("www.diariocambio.com.uy/?seccion={$section}");
			$xpath = $this->initialize();
			$c_n = $xpath->query("//div[@class='contenido_noticia']");
			foreach ($c_n as $childs) {
				foreach ($childs->childNodes as $value) {
					if($value->nodeName == 'p'){
						$data[$i]['body_short'] = trim(preg_replace("/([\w]*)  \| ([\d]*) ([\w]*)/i", "", utf8_decode($value->textContent)));
					}
					if($value->nodeName == 'div'){
						foreach ($value->childNodes as $v) {
							if($v->nodeName == 'a'){
								$data[$i]['link'] =  'http://www.diariocambio.com.uy/' . $v->getAttribute('href');
							}
						}		
					}
				}
				$i++;
			}

		}
		return $data;
	}
}