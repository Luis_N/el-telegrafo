<?php
class eltelegrafo extends parse implements PInterface{
	/*
		Convierte la fecha de fromato cadena a date YYYY/MM/DD H:i:s
		@return String
	*/
	private function fecha2time($fecha){
		$meses = array('Enero'=>01,'Febrero'=>02,'Marzo'=>03,'Abril'=>04,'Mayo'=>05,'Junio'=>06,'Julio'=>07,'Agosto'=>08,'Septiembre'=>09,'Octubre'=>10,'Noviembre'=>11,'Diciembre'=>12);
		preg_match("/Paysandú, (Lunes|Martes|Miércoles|Jueves|Viernes|Sábado|Domingo) ([0-9]{2}) de (Enero|Febrero|Marzo|Abril|Mayo|Junio|Julio|Agosto|Septiembre|Octubre|Noviembre|Diciembre) de ([0-9]{4})/", $fecha, $output_array);
		return $output_array[4] . '-' .$meses[$output_array[3]] . '-' . $output_array[2] . '2:30:00';
	}
	/*
		Obtiene la fecha desde el archivo fuente
	*/
	public function  getFecha($totime){
		$this->getSource("www.eltelegrafo.com");
		$xpath = $this->initialize();
		$fch = $xpath->query("//div[@id='fecha']");
		foreach ($fch->item(0)->childNodes as $fchchilds) {
			if($fchchilds->nodeName == 'p'){
				$fecha = trim(htmlspecialchars($fchchilds->nodeValue));
				break;
			}
		}
		if(!$fecha){
			throw new Exception('Formato de origen desconocido', E_USER_ERROR);
		}
		if($totime)
			return strtotime($this->fecha2time($fecha));
		return $this->fecha2time($fecha);;
	}
	public function getIndex(){
		$i=0;
		$xpath = $this->initialize();
		$items = $xpath->query("//div[@id='features']/div");
		foreach ($items as $item) {
			$body = $xpath->query('p',$item)->item(0);
			$this->data[$i]['body_short']= trim(htmlspecialchars($body->nodeValue));
			$link = $xpath->query('div/a',$item)->item(0);
			$this->data[$i]['link'] =  'http://www.eltelegrafo.com/' . $link->getAttribute('href');
			$i++;
		}
		$items = $xpath->query("//div[@class='contenido_noticia']");
		foreach ($items as $item) {
			if(!empty($item->nodeValue)){
				$body = $xpath->query('p', $item)->item(0);
				$this->data[$i]['body_short']= trim(htmlspecialchars($body->nodeValue));
				$link = $xpath->query('div/a', $item)->item(0);
				$this->data[$i]['link'] =  'http://www.eltelegrafo.com/' . $link->getAttributeNode('href')->value;	
				$i++;
			}
		}	
		
	}
	public function generate(){
		$as = array();
		$d = date("Y-m-d",strtotime($this->date));
		$this->getSource("www.eltelegrafo.com/?fechaedicion=$d");
		$this->getIndex();
		foreach($this->data as $data){
			$as[] = array_merge($this->getArticle($data['link']),$data);
		}
		return $as;
	}
	public function getArticle($url){
		$d = date("Y-m-d",strtotime($this->date));
		$this->getSource($url."&fechaedicion=$d");
		file_put_contents("urls.txt", $url."\r\n",FILE_APPEND);
		$xpath = $this->initialize();
		$contenido_noticia = $xpath->query("//div[@class='contenido_noticia']")->item(0);
		$title = $xpath->query("h2", $contenido_noticia)->item(0);
		$articulo['title'] = htmlspecialchars($title->nodeValue);
		$noticia = $xpath->query("div[@class='noticia']", $contenido_noticia)->item(0);
		$foto = @$xpath->query("div[@class='foto']",$noticia)->item(0);
		if($foto){
			$articulo['img'] = 'http://www.eltelegrafo.com/' . $foto->childNodes->item(0)->getAttribute('src');
		}
		$p =  $xpath->query("p", $noticia)->item(0);
		preg_match("/([\w]*)  \| ([\d]*) ([\w]*)/i", $p->textContent,$match);
		//
		$articulo['section'] =  trim($match[1]);
		$articulo['body'] = trim(preg_replace("/([\w]*)  \| ([\d]*) ([\w]*)/i", "", $p->textContent));
		$articulo['date'] = $this->date;
		return $articulo;
	}
}