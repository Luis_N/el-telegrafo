<?php
class Parse{
	protected $data = array();
	protected $date;
	private $ch;
	protected function getSource($url){
			if(!$this->ch){
				$this->ch = curl_init();
			}
			curl_setopt($this->ch, CURLOPT_URL, $url);
			curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($this->ch, CURLOPT_REFERER, 'http://www.eltelegrafo.com');
			curl_setopt($this->ch, CURLOPT_USERAGENT, 'Dalvik/1.4.0 (Linux; U; Android 2.3.7; LG-P350 Build/GRJ90)');
			curl_setopt($this->ch, CURLOPT_COOKIESESSION, true);
			curl_setopt($this->ch, CURLOPT_TIMEOUT, 60);
			$this->html = curl_exec($this->ch);
			if(curl_errno($this->ch) <> 0){
				curl_close($this->ch);
				throw new Exception('Error al obtener fuente de datos' . curl_error($this->ch), E_USER_ERROR);
			}
	}
	public function getDate($is8288=null){
		return ($is8288) ? date('r', strtotime($this->date . " " . date('H:i:s'))) : $this->date;
	}
	public function setDate($date){
		$this->date = $date;
	}
	public function __construct(){
		$this->date = date('Y-m-d');
	}
	/*
		Obtiene los datos analizados
		@return Array
	*/
	public function getData(){
		return $this->data;	
	}
	/*
		Inicia el parser XML
		@return DOMXpath
	*/
	protected function initialize(){
		$dom = new DOMDocument();
		if(!$dom->loadHTML($this->html)){
			throw new Exception('Error al procesar la fuente de datos: "' . $this->src .'"', E_USER_ERROR);
		}
			
		$xpath = new DOMXPath($dom);
		$xpath->registerNamespace('html','http://www.w3.org/1999/xhtml');
		return $xpath;
	}
	
	
}